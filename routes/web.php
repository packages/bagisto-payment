<?php

Route::middleware('web')
    ->namespace('DFM\Payment\Http\Controllers')
    ->prefix('e-transactions')
    ->name('e-transactions.')
    ->group(function () {

        Route::get('/redirect', 'ETransactionsController@redirect')
            ->name('redirect');

        Route::get('/cancel', 'ETransactionsController@cancel')
            ->name('cancel');

        Route::get('/success', 'ETransactionsController@success')
            ->middleware('throttle:1,1')
            ->name('success');

        Route::get('/failed', 'ETransactionsController@failed')
            ->name('failed');

        Route::get('/ipn', 'ETransactionsController@ipn')
            ->name('ipn');
});
