@inject('eTransactions', 'DFM\Payment\Payments\ETransactions3X')

<body data-gr-c-s-loaded="true" cz-shortcut-listen="true">
    You will be redirected to the E-Transactions website in a few seconds.

    <form action="{{ $eTransactions->getSystemUrl() }}" id="etransactions_standard_checkout" method="POST">
        <input value="Click here if you are not redirected within 10 seconds..." type="submit">
        @foreach ($eTransactions->buildSystemParams() as $key => $value)
            <input type="hidden" name="{{ $key }}" value="{{ $value }}">
        @endforeach
    </form>

    <script type="text/javascript">
        document.getElementById("etransactions_standard_checkout").submit();
    </script>
</body>
