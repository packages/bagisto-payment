<?php

return [
    'admin' => [
        'system' => [
            'e-transactions'        => 'E-Transactions',
            'e-transactions-3x'     => 'E-Transactions 3 times',
            'type'                  => 'Type',
            'delay'                 => 'Delay',
            '3ds'                   => '3D Secure',
            '3ds_enabled'           => 'Enable/Disable',
            'minimal_amount'        => 'Minimal amount',
            'etransactions_account' => 'E-Transactions account',
            'site_number'           => 'Site number',
            'rank_number'           => 'Rank number',
            'login'                 => 'Login',
            'hmac'                  => 'HMAC',
            'environment'           => 'Environment',
            'technical_settings'    => 'Technical settings',
            'allowed_ips'           => 'Allowed IPs',
            'bank-transfer'         => 'Bank Transfer',
            'instructions'          => 'Instructions',
        ],
    ],
];
