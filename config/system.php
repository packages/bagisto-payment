<?php

return [
    [
        'key'       => 'sales.paymentmethods.banktransfer',
        'name'      => 'dfm-payment::app.admin.system.bank-transfer',
        'sort'      => 4,
        'fields'    => [
            [
                'name'          => 'logo',
                'title'         => 'admin::app.admin.system.logo-image',
                'type'          => 'image',
            ],
            [
                'name'          => 'title',
                'title'         => 'admin::app.admin.system.title',
                'type'          => 'text',
                'validation'    => 'required',
                'channel_based' => false,
                'locale_based'  => true,
            ],
            [
                'name'          => 'description',
                'title'         => 'admin::app.admin.system.description',
                'type'          => 'textarea',
                'channel_based' => false,
                'locale_based'  => true,
            ],
            [
                'name'          => 'active',
                'title'         => 'admin::app.admin.system.status',
                'type'          => 'boolean',
                'validation'    => 'required',
                'channel_based' => false,
                'locale_based'  => true
            ],
            [
                'name'          => 'instructions',
                'title'         => 'dfm-payment::app.admin.system.instructions',
                'type'          => 'text-editor',
                'channel_based' => false,
                'locale_based'  => true,
            ],
            [
                'name'          => 'sort',
                'title'         => 'admin::app.admin.system.sort_order',
                'type'          => 'select',
            ],
        ]
    ],

    [
        'key'       => 'sales.paymentmethods.etransactions_std',
        'name'      => 'dfm-payment::app.admin.system.e-transactions',
        'sort'      => 5,
        'fields'    => [
            [
                'name'          => 'logo',
                'title'         => 'admin::app.admin.system.logo-image',
                'type'          => 'image',
            ],
            [
                'name'          => 'title',
                'title'         => 'admin::app.admin.system.title',
                'type'          => 'text',
                'validation'    => 'required',
                'channel_based' => false,
                'locale_based'  => true,
            ],
            [
                'name'          => 'description',
                'title'         => 'admin::app.admin.system.description',
                'type'          => 'textarea',
                'channel_based' => false,
                'locale_based'  => true,
            ],
            [
                'name'          => 'active',
                'title'         => 'admin::app.admin.system.status',
                'type'          => 'boolean',
                'validation'    => 'required',
                'channel_based' => false,
                'locale_based'  => true
            ],
            [
                'name'          => 'sort',
                'title'         => 'admin::app.admin.system.sort_order',
                'type'          => 'select',
            ],
            [
                'name'          => 'delay',
                'title'         => 'dfm-payment::app.admin.system.delay',
                'type'          => 'select',
                'options'       => [
                    [
                        'title'     => 'Immediate',
                        'value'     => 0,
                    ], [
                        'title'     => '1 day',
                        'value'     => 1,
                    ], [
                        'title'     => '2 days',
                        'value'     => 2,
                    ], [
                        'title'     => '3 days',
                        'value'     => 3,
                    ], [
                        'title'     => '4 days',
                        'value'     => 4,
                    ], [
                        'title'     => '5 days',
                        'value'     => 5,
                    ], [
                        'title'     => '6 days',
                        'value'     => 6,
                    ], [
                        'title'     => '7 days',
                        'value'     => 7,
                    ],
                ],
            ],
            [
                'name'          => 'amount',
                'title'         => 'dfm-payment::app.admin.system.minimal_amount',
                'type'          => 'text',
                'info'          => 'Enable this payment method for order with amount greater or equals to this amount (empty to ignore this condition).',
            ],
            [
                'name'          => '3ds',
                'title'         => 'dfm-payment::app.admin.system.3ds',
                'type'          => 'label',
            ],
            [
                'name'          => '3ds_enabled',
                'title'         => 'dfm-payment::app.admin.system.3ds_enabled',
                'type'          => 'select',
                'options'       => [
                    [
                        'title'     => 'Disabled',
                        'value'     => 'never',
                    ], [
                        'title'     => 'Enabled',
                        'value'     => 'always',
                    ], [
                        'title'     => 'Conditional',
                        'value'     => 'conditional',
                    ],
                ],
                'info'          => 'You can enable 3D Secure for all orders or depending on following conditions.',
            ],
            [
                'name'          => '3ds_amount',
                'title'         => 'dfm-payment::app.admin.system.minimal_amount',
                'type'          => 'text',
                'info'          => 'Enable 3D Secure for order with amount greater or equals to this amount (empty to ignore this condition).',
            ],
            [
                'name'          => 'etransactions_account',
                'title'         => 'dfm-payment::app.admin.system.etransactions_account',
                'type'          => 'label',
            ],
            [
                'name'          => 'site',
                'title'         => 'dfm-payment::app.admin.system.site_number',
                'type'          => 'text',
                'info'          => 'Site number provided by E-Transactions.',
            ],
            [
                'name'          => 'rank',
                'title'         => 'dfm-payment::app.admin.system.rank_number',
                'type'          => 'text',
                'info'          => 'Rank number provided by E-Transactions (two last digits).',
            ],
            [
                'name'          => 'identifier',
                'title'         => 'dfm-payment::app.admin.system.login',
                'type'          => 'text',
                'info'          => 'Internal login provided by E-Transactions.',
            ],
            [
                'name'          => 'hmackey',
                'title'         => 'dfm-payment::app.admin.system.hmac',
                'type'          => 'text',
                'info'          => 'Secrete HMAC key to create using the E-Transactions interface.',
            ],
            [
                'name'          => 'environment',
                'title'         => 'dfm-payment::app.admin.system.environment',
                'type'          => 'select',
                'options'       => [
                    [
                        'title'     => 'Production',
                        'value'     => 'PRODUCTION',
                    ], [
                        'title'     => 'Test',
                        'value'     => 'TEST',
                    ],
                ],
                'info'          => 'In test mode your payments will not be sent to the bank.',
            ],
        ]
    ],

    [
        'key'       => 'sales.paymentmethods.etransactions_3x',
        'name'      => 'dfm-payment::app.admin.system.e-transactions-3x',
        'sort'      => 6,
        'fields'    => [
            [
                'name'          => 'logo',
                'title'         => 'admin::app.admin.system.logo-image',
                'type'          => 'image',
            ],
            [
                'name'          => 'title',
                'title'         => 'admin::app.admin.system.title',
                'type'          => 'text',
                'validation'    => 'required',
                'channel_based' => false,
                'locale_based'  => true,
            ],
            [
                'name'          => 'description',
                'title'         => 'admin::app.admin.system.description',
                'type'          => 'textarea',
                'channel_based' => false,
                'locale_based'  => true,
            ],
            [
                'name'          => 'active',
                'title'         => 'admin::app.admin.system.status',
                'type'          => 'boolean',
                'validation'    => 'required',
                'channel_based' => false,
                'locale_based'  => true
            ],
            [
                'name'          => 'sort',
                'title'         => 'admin::app.admin.system.sort_order',
                'type'          => 'select',
            ],
            [
                'name'          => 'amount',
                'title'         => 'dfm-payment::app.admin.system.minimal_amount',
                'type'          => 'text',
                'info'          => 'Enable this payment method for order with amount greater or equals to this amount (empty to ignore this condition).',
            ],
            [
                'name'          => '3ds',
                'title'         => 'dfm-payment::app.admin.system.3ds',
                'type'          => 'label',
            ],
            [
                'name'          => '3ds_enabled',
                'title'         => 'dfm-payment::app.admin.system.3ds_enabled',
                'type'          => 'select',
                'options'       => [
                    [
                        'title'     => 'Disabled',
                        'value'     => 'never',
                    ], [
                        'title'     => 'Enabled',
                        'value'     => 'always',
                    ], [
                        'title'     => 'Conditional',
                        'value'     => 'conditional',
                    ],
                ],
                'info'          => 'You can enable 3D Secure for all orders or depending on following conditions.',
            ],
            [
                'name'          => '3ds_amount',
                'title'         => 'dfm-payment::app.admin.system.minimal_amount',
                'type'          => 'text',
                'info'          => 'Enable 3D Secure for order with amount greater or equals to this amount (empty to ignore this condition).',
            ],
            [
                'name'          => 'etransactions_account',
                'title'         => 'dfm-payment::app.admin.system.etransactions_account',
                'type'          => 'label',
            ],
            [
                'name'          => 'site',
                'title'         => 'dfm-payment::app.admin.system.site_number',
                'type'          => 'text',
                'info'          => 'Site number provided by E-Transactions.',
            ],
            [
                'name'          => 'rank',
                'title'         => 'dfm-payment::app.admin.system.rank_number',
                'type'          => 'text',
                'info'          => 'Rank number provided by E-Transactions (two last digits).',
            ],
            [
                'name'          => 'identifier',
                'title'         => 'dfm-payment::app.admin.system.login',
                'type'          => 'text',
                'info'          => 'Internal login provided by E-Transactions.',
            ],
            [
                'name'          => 'hmackey',
                'title'         => 'dfm-payment::app.admin.system.hmac',
                'type'          => 'text',
                'info'          => 'Secrete HMAC key to create using the E-Transactions interface.',
            ],
            [
                'name'          => 'environment',
                'title'         => 'dfm-payment::app.admin.system.environment',
                'type'          => 'select',
                'options'       => [
                    [
                        'title'     => 'Production',
                        'value'     => 'PRODUCTION',
                    ], [
                        'title'     => 'Test',
                        'value'     => 'TEST',
                    ],
                ],
                'info'          => 'In test mode your payments will not be sent to the bank.',
            ],
        ]
    ],
];
