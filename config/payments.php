<?php

return [
    'banktransfer' => [
        'code'          => 'banktransfer',
        'title'         => 'Bank Transfer',
        'description'   => 'Bank Transfer',
        'class'         => 'DFM\Payment\Payments\BankTransfer',
        'active'        => true,
        'instructions'  => <<<HTML
<div>@include("shop::checkout.onepage.address")</div>
<table>
    <tbody>
        <tr>
            <td>IBAN:</td>
            <td>FR76 1670 6000 2053 9645 0433 016</td>
        </tr>
        <tr>
            <td>Code BIC/SWIFT:</td>
            <td>AGRIFRPP867</td>
        </tr>
    </tbody>
    <tbody>
        <tr>
            <td>Titulaire:</td>
            <td>DAREWIN STORE</td>
        </tr>
        <tr>
            <td>Numéro de compte:</td>
            <td>53964504330</td>
        </tr>
        <tr>
            <td>Clé:</td>
            <td> 16</td></tr>
        <tr>
            <td>Nom de la banque:</td>
            <td>Crédit Agricole Nord de France</td>
        </tr>
    </tbody>
</table>
HTML,
        'sort'          => 4,
    ],

    'etransactions_std' => [
        'code'          => 'etransactions_std',
        'title'         => 'E-Transactions',
        'description'   => 'E-Transactions is a Payment Services Provider in Europe, part of the Crédit Agricole Bank.',
        'class'         => 'DFM\Payment\Payments\ETransactions',
        'delay'         => 0,
        'amount'        => '',
        'active'        => true,
        'sort'          => 5,
        '3ds_enabled'   => 'never',
        '3ds_amount'    => '',
        'site'          => '',
        'rank'          => '',
        'identifier'    => '',
        'hmackey'       => '',
        'environment'   => 'TEST',
    ],

    'etransactions_3x' => [
        'code'          => 'etransactions_3x',
        'title'         => 'E-Transactions 3 times payment',
        'description'   => 'E-Transactions is a Payment Services Provider in Europe, part of the Crédit Agricole Bank.',
        'class'         => 'DFM\Payment\Payments\ETransactions3X',
        'amount'        => '',
        'active'        => true,
        'sort'          => 6,
        '3ds_enabled'   => 'never',
        '3ds_amount'    => '',
        'site'          => '',
        'rank'          => '',
        'identifier'    => '',
        'hmackey'       => '',
        'environment'   => 'TEST',
    ],
];
