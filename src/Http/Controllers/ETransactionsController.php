<?php

namespace DFM\Payment\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Webkul\Checkout\Facades\Cart;
use Webkul\Sales\Repositories\OrderRepository;

class ETransactionsController extends Controller
{
    /**
     * @var OrderRepository
     */
    protected $orderRepository;

    /**
     * ETransactionsController constructor.
     *
     * @param  OrderRepository  $orderRepository
     */
    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    /**
     * Redirects to the E-Transactions.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function redirect(Request $request)
    {
        return view("dfm-payment::e-transactions.{$request->get('type')}-redirect");
    }

    /**
     * Cancel payment from E-Transactions.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function cancel()
    {
        session()->flash('error', 'E-Transactions payment has been canceled.');

        return redirect()->route('shop.checkout.cart.index');
    }

    /**
     * Success payment
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function success()
    {
        if (! ($cart = Cart::getCart()) || ! $cart->is_active) {
            session()->flash('warning', 'The cart is empty.');

            return redirect()->route('shop.checkout.cart.index');
        }

        if ($this->orderRepository->whereCartId($cart->id)->exists()) {
            $order = $this->orderRepository->whereCartId($cart->id)->orderByDesc('id')->first();
        } else {
            $order = $this->orderRepository->create(Cart::prepareDataForOrder());
        }

        Cart::deActivateCart();

        session()->flash('order', $order);

        return redirect()->route('shop.checkout.success');
    }

    /**
     * Failed by E-Transactions.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function failed()
    {
        session()->flash('error', 'E-Transactions payment failed.');

        return redirect()->route('shop.checkout.cart.index');
    }

    /**
     * E-Transactions Ipn listener
     */
    public function ipn()
    {
        //
    }
}
