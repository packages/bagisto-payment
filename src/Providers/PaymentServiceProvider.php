<?php

namespace DFM\Payment\Providers;

use Illuminate\Support\ServiceProvider;

class PaymentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../../config/payments.php', 'paymentmethods'
        );

        $this->mergeConfigFrom(
            __DIR__.'/../../config/system.php', 'core'
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../../routes/web.php');
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
        $this->loadTranslationsFrom(__DIR__.'/../../resources/lang', 'dfm-payment');
        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'dfm-payment');

        $this->publishes([
            __DIR__.'/../../resources/data' => resource_path('data'),
        ], 'payment-data');
    }
}
