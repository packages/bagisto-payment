<?php

namespace DFM\Payment\Services;

class ISO4217Currency
{
    /**
     * @var \Illuminate\Support\Collection
     */
    private $codeList;

    /**
     * ISO4217Currency constructor.
     */
    public function __construct()
    {
        $this->codeList = $this->getCodeList();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public static function getCodeList()
    {
        $array = [];

        $filePath = resource_path('data/iso4217/list_one.xml');
        $urlPath = 'https://www.currency-iso.org/dam/downloads/lists/list_one.xml';

        if ($data = (@simplexml_load_file($filePath) ?: @simplexml_load_file($urlPath))) {
            foreach ($data->CcyTbl->CcyNtry as $item) {
                array_push($array, (array) $item);
            }
        }

        return collect($array);
    }

    /**
     * @param  string  $code
     * @return \Illuminate\Support\Collection
     */
    public function getByCode(string $code)
    {
        return $this->codeList->where('Ccy', strtoupper($code));
    }

    /**
     * @param  string|int  $number
     * @return \Illuminate\Support\Collection
     */
    public function getByNumeric($number)
    {
        return $this->codeList->where('CcyNbr', $number);
    }

    /**
     * @param  string|int  $number
     * @return mixed
     */
    public function getCodeByNumeric($number)
    {
        return @($this->codeList->firstWhere('CcyNbr', $number)['Ccy']);
    }

    /**
     * @param  string  $code
     * @return mixed
     */
    public function getNumericByCode(string $code)
    {
        return @($this->codeList->firstWhere('Ccy', strtoupper($code))['CcyNbr']);
    }

    /**
     * @param  string  $code
     * @return mixed
     */
    public function getMinorUnitByCode(string $code)
    {
        return @($this->codeList->firstWhere('Ccy', strtoupper($code))['CcyMnrUnts']);
    }

    /**
     * @param  string|int  $number
     * @return mixed
     */
    public function getMinorUnitByNumeric($number)
    {
        return @($this->codeList->firstWhere('CcyNbr', $number)['CcyMnrUnts']);
    }
}
