<?php

namespace DFM\Payment\Payments;

class ETransactions3X extends ETransactionsAbstract
{
    /**
     * Payment method code
     *
     * @var string
     */
    protected $code = 'etransactions_3x';

    /**
     * @return string
     */
    public function getRedirectUrl()
    {
        return route('e-transactions.redirect', ['type' => '3times']);
    }
}
