<?php

namespace DFM\Payment\Payments;

use DFM\Payment\Services\ISO4217Currency;
use Webkul\Payment\Payment\Payment;

abstract class ETransactionsAbstract extends Payment
{
    /**
     * @var ISO4217Currency
     */
    protected $iso4217;

    /**
     * ETransactionsAbstract constructor.
     *
     * @param  ISO4217Currency  $iso4217
     */
    public function __construct(ISO4217Currency $iso4217)
    {
        $this->iso4217 = $iso4217;
    }

    /**
     * @return array
     * @throws \Exception
     */
    public function buildSystemParams()
    {
        $cart = $this->getCart();
        $additionalParams = [
            'PBX_ANNULE'     => route('e-transactions.cancel'),
            'PBX_EFFECTUE'   => route('e-transactions.success'),
            'PBX_REFUSE'     => route('e-transactions.failed'),
            'PBX_REPONDRE_A' => route('e-transactions.ipn'),
        ];

        // Parameters
        $values = [];

        // Merchant information
        $values['PBX_SITE'] = $this->getConfigData('site');
        $values['PBX_RANG'] = $this->getConfigData('rank');
        $values['PBX_IDENTIFIANT'] = $this->getConfigData('identifier');

        // Order information
        $values['PBX_PORTEUR'] = $cart->customer_email;
        $values['PBX_DEVISE'] = $this->iso4217->getNumericByCode($cart->cart_currency_code);
        $values['PBX_CMD'] = "{$cart->id} - {$cart->customer_first_name} {$cart->customer_last_name}";

        // Amount
        $orderAmount = $cart->grand_total;
        $amountScale = $this->iso4217->getMinorUnitByCode($cart->cart_currency_code);
        $amountScale = pow(10, $amountScale);

        switch ($type = $this->code) {
            case 'etransactions_std':
                if (($delay = $this->getConfigData('delay')) > 0) {
                    if ($delay > 7) {
                        $delay = 7;
                    }
                    $values['PBX_DIFF'] = sprintf('%02d', $delay);
                }
                $values['PBX_TOTAL'] = sprintf('%03d', round($orderAmount * $amountScale));
                break;

            case 'etransactions_3x':
                // Compute each payment amount
                $step = round($orderAmount * $amountScale / 3);
                $firstStep = ($orderAmount * $amountScale) - 2 * $step;
                $values['PBX_TOTAL'] = sprintf('%03d', $firstStep);
                $values['PBX_2MONT1'] = sprintf('%03d', $step);
                $values['PBX_2MONT2'] = sprintf('%03d', $step);

                // Payment dates
                $now = new \DateTime();
                $now->modify('1 month');
                $values['PBX_DATE1'] = $now->format('d/m/Y');
                $now->modify('1 month');
                $values['PBX_DATE2'] = $now->format('d/m/Y');

                // Force validity date of card
                $values['PBX_DATEVALMAX'] = $now->format('ym');
                break;

            default:
                throw new \Exception("Unexpected type {$type}");
        }

        // 3D Secure
        switch ($this->getConfigData('3ds_enabled')) {
            case 'never':
                $enable3ds = false;
                break;
            case null:
            case 'always':
                $enable3ds = true;
                break;
            case 'conditional':
                $tdsAmount = $this->getConfigData('3ds_amount');
                $enable3ds = empty($tdsAmount) || ($orderAmount >= $tdsAmount);
                break;
            default:
                throw new \Exception('Unexpected 3-D Secure status');
        }
        // Enable is the default behaviour
        if (!$enable3ds) {
            $values['PBX_3DS'] = 'N';
        }

        // E-Transactions => Magento
        $values['PBX_RETOUR'] = 'M:M;R:R;T:T;A:A;B:B;C:C;D:D;E:E;F:F;G:G;I:I;J:J;N:N;O:O;P:P;Q:Q;S:S;W:W;Y:Y;K:K';
        $values['PBX_RUF1'] = 'POST';

        // Choose correct language
        $lang = app()->getLocale();
        if (!empty($lang)) {
            $lang = preg_replace('#_.*$#', '', $lang);
        }
        $languages = $this->getLanguages();
        if (!array_key_exists($lang, $languages)) {
            $lang = 'default';
        }
        $values['PBX_LANGUE'] = $languages[$lang];

        // Choose page format depending on browser/devise
        if ($this->isMobile()) {
            $values['PBX_SOURCE'] = 'XHTML';
        }

        // Misc.
        $values['PBX_TIME'] = date('c');
        $values['PBX_HASH'] = strtoupper($this->getHmacAlgo());

        // Adding additionnal informations
        $values = array_merge($values, $additionalParams);

        // Sort parameters for simpler debug
        ksort($values);

        // Sign values
        $sign = $this->signValues($values);

        // Hash HMAC
        $values['PBX_HMAC'] = $sign;

        return $values;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getSystemUrl()
    {
        $urls = $this->getSystemUrls();
        if (empty($urls)) {
            $message = 'Missing URL for E-Transactions system in configuration';
            throw new \Exception($message);
        }

        // look for valid peer
        foreach ($urls as $url) {
            $doc = new \DOMDocument();
            $testUrl = preg_replace('#^([a-zA-Z0-9]+://[^/]+)(/.*)?$#', '\1/load.html', $url);

            if (@$doc->loadHTMLFile($testUrl) && @($doc->getElementById('server_status')->textContent == 'OK')) {
                return $url;
            }
        }

        // Here, there's a problem
        throw new \Exception(__('E-Transactions not available. Please try again later.'));
    }

    /**
     * @return string[]
     */
    public function getSystemUrls()
    {
        if ($this->isProduction()) {
            return $this->getSystemProductionUrls();
        }
        return $this->getSystemTestUrls();
    }

    /**
     * @return string[]
     */
    public function getSystemProductionUrls()
    {
        return [
            'https://tpeweb.e-transactions.fr/cgi/MYchoix_pagepaiement.cgi',
            'https://tpeweb1.e-transactions.fr/cgi/MYchoix_pagepaiement.cgi',
        ];
    }

    /**
     * @return string[]
     */
    public function getSystemTestUrls()
    {
        return [
            'https://preprod-tpeweb.e-transactions.fr/cgi/MYchoix_pagepaiement.cgi'
        ];
    }

    /**
     * @return bool
     */
    public function isProduction()
    {
        return $this->getConfigData('environment') === 'PRODUCTION';
    }

    /**
     * @return string[]
     */
    public function getLanguages()
    {
        return array(
            'fr' => 'FRA',
            'es' => 'ESP',
            'it' => 'ITA',
            'de' => 'DEU',
            'nl' => 'NLD',
            'sv' => 'SWE',
            'pt' => 'PRT',
            'default' => 'GBR',
        );
    }

    /**
     * @param  array  $values
     *
     * @return string
     * @throws \Exception
     */
    public function signValues(array $values)
    {
        // Serialize values
        $query = urldecode(http_build_query($values));

        // Prepare key
        $key = pack('H*', $this->getConfigData('hmackey'));

        // Sign values
        $sign = hash_hmac($this->getHmacAlgo(), $query, $key);
        if (!$sign) {
            throw new \Exception('Unable to create hmac signature. Maybe a wrong configuration.');
        }

        return strtoupper($sign);
    }

    /**
     * @return string
     */
    public function getHmacAlgo()
    {
        return 'SHA512';
    }

    /**
     * @return bool
     */
    public function isMobile()
    {
        // From http://detectmobilebrowsers.com/, regexp of 09/09/2013
        global $_SERVER;
        $userAgent = $_SERVER['HTTP_USER_AGENT'];
        if (preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$userAgent)) {
            return true;
        }
        if (preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i', substr($userAgent, 0, 4))) {
            return true;
        }
        return false;
    }
}
