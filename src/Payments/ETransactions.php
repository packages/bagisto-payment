<?php

namespace DFM\Payment\Payments;

class ETransactions extends ETransactionsAbstract
{
    /**
     * Payment method code
     *
     * @var string
     */
    protected $code = 'etransactions_std';

    /**
     * @return string
     */
    public function getRedirectUrl()
    {
        return route('e-transactions.redirect', ['type' => 'standard']);
    }
}
